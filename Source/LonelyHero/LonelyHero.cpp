// Copyright Epic Games, Inc. All Rights Reserved.

#include "LonelyHero.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, LonelyHero, "LonelyHero" );

DEFINE_LOG_CATEGORY(LogLonelyHero)
 