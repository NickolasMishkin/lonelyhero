// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AbilityComponent.generated.h"

class ALonelyHeroGameMode;
class ABaseCharacter;
class ALonelyHeroPlayerController;
class UAblityBase;

UCLASS()
class UAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UAbilityComponent();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UAblityBase*> Abilities;

protected:

	virtual void BeginPlay() override;

private:
	TWeakObjectPtr<ABaseCharacter> CachedBaseCharacterOwner;

	UPROPERTY()
	FTimerHandle AbilityTimer;

	UPROPERTY()
	ALonelyHeroPlayerController* PlayerController = nullptr;

	UPROPERTY()
	ALonelyHeroGameMode* GameMode = nullptr;
};
