// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AttributeComponent.generated.h"

class ABaseCharacter;

DECLARE_MULTICAST_DELEGATE(FOnDeathSignature);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHealthChangedSignature, float);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UAttributeComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UAttributeComponent();

	FOnDeathSignature OnDeathEvent;
	FOnHealthChangedSignature OnHealthChangedEvent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Experience");
	float Experience = 500.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Experience");
	int32 Level = 1;

	UPROPERTY(EditDefaultsonly, Category = "Stats")
	float MaxHealth = 1700;

	float Health;

	UPROPERTY(EditDefaultsonly, Category = "Stats")
	float MinAttack = 300.f;

	UPROPERTY(EditDefaultsonly, Category = "Stats")
	float MaxAttack = 800.f;

	UPROPERTY(EditDefaultsonly, Category = "Stats", meta = (UIMin = 0, UIMax = 1))
	float BLockPercent = 0.7f;

	UPROPERTY(EditDefaultsonly, Category = "Stats", meta = (UIMin = 0, UIMax = 1))
	float Accuracy = 0.7f;

	UPROPERTY(EditDefaultsonly, Category = "Stats", meta = (UIMin = 0, UIMax = 100));
	int32 CritChance = 10;

	UPROPERTY(EditDefaultsonly, Category = "Stats", meta = (UIMin = 0, UIMax = 100));
	int32 StunChance = 40;

	UPROPERTY(EditDefaultsonly, Category = "Stats");
	float PoisonDamage = 100.f;

	bool bIsDefence = false;

public:

//	void Retreat();
//
	bool IsAlive() const { return Health > 0.f; }
	float GetHealthPercent() const { return Health / MaxHealth; }
//
//	void SetAttack(float NewAttack) { MinAttack += NewAttack; MaxAttack += NewAttack; }
	void UpdateHealth(float HP) { Health = Health + HP >= MaxHealth ? MaxHealth : Health + HP; OnHealthChangedEvent.Broadcast(0); }
//	void SetArmor(float NewArmor) { BLockPercent += NewArmor; }
//	void SetCritChance(float NewCritChance) { CritChance += NewCritChance; }
//	void SetAccuracy(float NewAccuracy) { Accuracy += NewAccuracy; }
//
//	void UpdateExperience(float Exp);
//	void UpgradeLevel(int32 NewLevel);
//
//	void Init(const FCharacterAtr& Attributes, int32 InCurHp, int32 InMaxHP);
//
//protected:
//	virtual void BeginPlay() override;
//
private:
	TWeakObjectPtr<ABaseCharacter> CachedBaseCharacterOwner;
//
//	UFUNCTION()
//		void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
//
//	void OnHealthChanged(float Damage);

};
