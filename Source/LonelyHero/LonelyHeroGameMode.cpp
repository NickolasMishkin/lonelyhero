// Copyright Epic Games, Inc. All Rights Reserved.

#include "LonelyHeroGameMode.h"
#include "LonelyHeroPlayerController.h"
#include "LonelyHeroCharacter.h"
#include "UObject/ConstructorHelpers.h"

ALonelyHeroGameMode::ALonelyHeroGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ALonelyHeroPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}