// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LonelyHeroGameMode.generated.h"

UCLASS(minimalapi)
class ALonelyHeroGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALonelyHeroGameMode();
};



